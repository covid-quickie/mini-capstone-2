


document.querySelector("#patientName").innerHTML = localStorage.getItem("participant")
document.querySelector("#patientAge").innerHTML = localStorage.getItem("age")
document.querySelector("#result1").innerHTML = localStorage.getItem("symptoms")
document.querySelector("#result2").innerHTML = localStorage.getItem("history of exposure")
document.querySelector("#result3").innerHTML = localStorage.getItem("status")



document.querySelector("#link").addEventListener("click", (e) => {

	e.preventDefault();
	localStorage.clear();
	window.location.href="index.html"
})
