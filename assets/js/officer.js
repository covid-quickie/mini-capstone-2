//get started button
document.querySelector("#startButton").addEventListener("click", function() {
	document.querySelector("#modal-title").style.display = "none";
	document.querySelector("#modal-title2").style.display = "block";
})

let examinee;
let examAge;

// register done button
document.querySelector("#proceed0").addEventListener("click", function() {
	let examinee = document.querySelector("#zeroA1").value;
	let examAge = parseInt(document.querySelector("#zeroA2").value)


	if(examinee.length < 2)
	{
		document.querySelector("#error").style.display = "block";
	} 
	else if (examAge > 100 || examAge == isNaN)
	{
		document.querySelector("#fact1").style.display = "none";
		document.querySelector("#error").style.display = "none";
		document.querySelector("#error1").style.display = "block";
	}
	else /*if (examAge < 100 && examinee.length >= 2) */
	{
		document.querySelector("#zero").style.display = "none";
		document.querySelector("#first").style.display = "block";
		document.querySelector("#modal-title").style.display = "block";
		document.querySelector("#modal-title2").style.display = "none";
		localStorage.setItem("participant",examinee);
		localStorage.setItem("age",examAge);
	}
})

// 1st click function
document.querySelector("#proceed1").addEventListener("click", function() {
	let a = document.querySelector("#firstA1")
	let b = document.querySelector("#firstA2")
	
	if(a.checked==true) {
		document.querySelector("#first").style.display = "none";
		document.querySelector("#second").style.display = "block";
		localStorage.setItem("travel history","traveled within 14 days? " + a.value );		
	} 
	else if (b.checked==true)
	{
		document.querySelector("#first").style.display = "none";
		document.querySelector("#fourth").style.display = "block";
		localStorage.setItem("travel history","traveled within 14 days? " + b.value );		
	}
})

// 2nd click function
document.querySelector("#proceed4").addEventListener("click", function() {
	let c = document.querySelector("#fourthA1").value;
	let c0 = document.querySelector("#c0").value;
	let c1 = document.querySelector("#c1").value;
	let c2 = document.querySelector("#c2").value;
	let c3 = document.querySelector("#c3").value;
	let c4 = document.querySelector("#c4").value;
	let c5 = document.querySelector("#c5").value;

	if(c == c1 || c == c2 || c== c3 || c==c4) {
		document.querySelector("#second").style.display = "block";
		document.querySelector("#fourth").style.display = "none";
		localStorage.setItem("history of exposure","evaluation:" + " has positive exposure." );
	}
	else if (c == c5)
	{
		document.querySelector("#goodnews").style.display = "block";
		document.querySelector("#fourth").style.display = "none";
		localStorage.setItem("history of exposure","evaluation:" + " no exposure." );
		localStorage.setItem("status"," Neither PUM or PUI" );
	}
	else if (c == c0)
	{
		document.querySelector("#hello").innerHTML = `Please select a proper choice`;
		document.querySelector("#hello").style.color = "indianred"		
	}

})

// 3rd click function
document.querySelector("#proceed2").addEventListener("click", function() {
	let d = document.querySelector("#secondA1")
	let e = document.querySelector("#secondA2")
	let dd = document.querySelector("#secondA3")
	let ee = document.querySelector("#secondA4")

	if(e.checked==true || dd.checked==true) {
		document.querySelector("#second").style.display = "none";
		document.querySelector("#badnews").style.display = "block";
		localStorage.setItem("symptoms","fever and/or cold" );
		localStorage.setItem("status"," PUI (Person under investigation)" );
	} 
	else if (d.checked==true)
	{
		document.querySelector("#second").style.display = "none";
		document.querySelector("#third").style.display = "block";
		localStorage.setItem("symptoms","fever" );
	}
	else if (ee.checked==true)
	{
		document.querySelector("#second").style.display = "none";
		document.querySelector("#neutralbadnews").style.display = "block";
		localStorage.setItem("symptoms","none" );
		localStorage.setItem("status"," PUM (Person under monitoring)" );
	}


})

// 4th click function
document.querySelector("#proceed3").addEventListener("click", function() {
	let f = document.querySelector("#thirdA1").value;

	if (f.length == 0)
	{
		document.querySelector("#hi").innerHTML = `please input a number`;
	}
	else if (f <= 38 && f  > 33 ) 
	{
		document.querySelector("#third").style.display = "none";
		document.querySelector("#neutralbadnews").style.display = "block";
		localStorage.setItem("bodytemp", f );
		localStorage.setItem("symptoms", "no fever");
		localStorage.setItem("status"," PUM (Person under monitoring)" );
	}
	else if (f > 38 && f < 43)
	{
		document.querySelector("#third").style.display = "none";
		document.querySelector("#badnews").style.display = "block";
		localStorage.setItem("bodytemp", f);
		localStorage.setItem("status"," PUI (Person under investigation)" )
	}
	else 
	{	
		document.querySelector("#fact").style.display = "none";
		document.querySelector("#guide").style.display = "block";
		document.querySelector("#guide2").style.display = "block";
		document.querySelector("#helllo").innerHTML = `Are you serious?`;
		document.querySelector("#ifyes").innerHTML = `If yes:`;
		document.querySelector("#ifyes").style.fontWeight = `bold`;
		document.querySelector("#yesEx").innerHTML = `Please immediately consult with an Endocrinologists! your life is in grave danger!`;
		document.querySelector("#ifno").innerHTML = `If no:`;
		document.querySelector("#ifno").style.fontWeight = `bold`;
		document.querySelector("#noEx").innerHTML = `please be serious and input your actual body temperature.`;
		document.querySelector("#helllo").style.color = "red";
		document.querySelector("#helllo").style.fontSize = "28px"
	}



})

		document.querySelector("#getresult1").addEventListener("click", () =>{
		window.location.href="result.html"})

		document.querySelector("#getresult2").addEventListener("click", () =>{
		window.location.href="result.html"})

		document.querySelector("#getresult3").addEventListener("click", () =>{
		window.location.href="result.html"})
	

