let next = document.querySelector('#next')

let body = document.querySelector('#body')
let body1 = document.querySelector('#body1')
let body2 = document.querySelector('#body2')
let body3 = document.querySelector('#body3')
let body4 = document.querySelector('#body4')

let result1 = document.querySelector('#result1')
let result2 = document.querySelector('#result2')
let result3 = document.querySelector('#result3')

let buttonContainer = document.querySelector('#button-container')

let name = document.querySelector('#name')

let age = document.querySelector('#age')

next.addEventListener('click', ()=>{
	localStorage.setItem("participant", name.value);
	localStorage.setItem("age", age.value)
	body.style.display = "none"
	body1.style.display = "block"
	if(document.querySelector('#fever1').checked == true) {localStorage.setItem("symptoms", document.querySelector('#fever').innerHTML)}
	if(document.querySelector('#cough1').checked == true) {localStorage.setItem("symptoms", document.querySelector('#cough').innerHTML)}
	if(document.querySelector('#breath1').checked == true) {localStorage.setItem("symptoms", document.querySelector('#breath').innerHTML)}
	let none = document.querySelector('#none')

	next.addEventListener('click', ()=>{
		if(none.checked == true) {
			body1.style.display = "none"
			result1.style.display = "block"
			localStorage.setItem("symptoms", "None")
			localStorage.setItem("status", document.querySelector('#pum').innerHTML)
			buttonContainer.innerHTML = '<a href="./result.html"><button id="next" type="button" class="btn btn-info">Next</button></a>'

		} else {
			body1.style.display = "none"
			body2.style.display = "block"
			let none2 = document.querySelector('#none2')
			if(document.querySelector('#travel1').checked == true) {localStorage.setItem("history of exposure", document.querySelector('#travel').innerHTML)}
			if(document.querySelector('#direct1').checked == true) {localStorage.setItem("history of exposure", document.querySelector('#direct').innerHTML)}
			if(document.querySelector('#stay1').checked == true) {localStorage.setItem("history of exposure", document.querySelector('#stay').innerHTML)}
			if(document.querySelector('#close1').checked == true) {localStorage.setItem("history of exposure", document.querySelector('#close').innerHTML)}

			next.addEventListener('click', ()=> {
				if(none2.checked == true) {
					localStorage.setItem("history of exposure", "None")
					body2.style.display = "none"
					body3.style.display = "block"
					let yes = document.querySelector('#yes')

					next.addEventListener('click', ()=>{
						if(yes.checked == true) {
							body3.style.display = "none"
							result2.style.display = "block"
							localStorage.setItem("status", document.querySelector('#pui').innerHTML)
							buttonContainer.innerHTML = '<a href="./result.html"><button id="next" type="button" class="btn btn-info">Next</button></a>'

						} else {
							body3.style.display = "none"
							result3.style.display = "block"
							localStorage.setItem("status", document.querySelector('#neither').innerHTML)
							buttonContainer.innerHTML = '<a href="./result.html"><button id="next" type="button" class="btn btn-info">Next</button></a>'
						}
					})

				}else {				
					body2.style.display = "none"
					body4.style.display = "block"
					let yes2 = document.querySelector('#yes2')

					next.addEventListener('click', ()=> {
						if(yes2.checked == true) {
							body4.style.display = "none"
							result2.style.display = "block"
							localStorage.setItem("status", document.querySelector('#pui').innerHTML)
							buttonContainer.innerHTML = '<a href="./result.html"><button id="next" type="button" class="btn btn-info">Next</button></a>'

						} else {
							body4.style.display = "none"
							result3.style.display = "block"
							localStorage.setItem("status", document.querySelector('#neither').innerHTML)
							buttonContainer.innerHTML = '<a href="./result.html"><button id="next" type="button" class="btn btn-info">Get Results!</button></a>'
						}
					})
				}
			})
			}
	})	
})











// let next = document.querySelector('#next')

// let body = document.querySelector('#body')

// let buttonContainer = document.querySelector('#button-container')

// let name = document.querySelector('#name')

// let age = document.querySelector('#age')

// next.addEventListener('click', ()=>{
// 	localStorage.setItem("name", name.value);
// 	localStorage.setItem("age", age.value)
// 	body.innerHTML = '<p>1. Does the patient have one of these illness</p> 																															<div><input id="fever1" type="radio" name="group1"><span id="fever"> Fever</span></div>																							<div><input id="cough1" type="radio" name="group1"><span id="cough"> Cough</span></div>																							<div><input id="breath1" type="radio" name="group1"><span id="breath"> Shortness of breath</span></div>																			<div><input id="none" type="radio" name="group1"> None of the above</div>'
// 	let none = document.querySelector('#none')

// 	next.addEventListener('click', ()=>{
// 		if(none.checked == true) {
// 			body.innerHTML = '<h4>Your status is:</h4><p id="pum" class="text-center">Person Under Monitoring</p>'
// 			localStorage.setItem("status", document.querySelector('#pum').innerHTML)
// 			buttonContainer.innerHTML = '<a href="./result.html"><button id="next" type="button" class="btn btn-primary">Next</button></a>'

// 		} else {
// 			document.querySelector('#body').innerHTML = '<p>2. Have any one of these happened?</p> 																							<div><input type="radio" name="group2"><span id="travel"> Travel to or residence in a country/area reporting local transmission of COVID-19</span></div>				<div><input type="radio" name="group2"><span id="direct"> Providing direct care without propper PPE to confirmed COVID-19 patient</span></div>							<div><input type="radio" name="group2"><span id="stay"> Staying in the same close environment (inl. workplace, classroom, household, gatherings)</span></div>			<div><input type="radio" name="group2"><span id="close"> Traveling together in close proximity (1 meter or 3 feet) in any kind of conveyance</span></div>				<div><input id="none2" type="radio" name="group2"> None of the above</div>'
// 			let none2 = document.querySelector('#none2')

// 			next.addEventListener('click', ()=> {
// 				if(none2.checked == true) {
					
// 					body.innerHTML = '<p>3. Does the patient have respiratory infection or atypical pneumonia? regardless of exposure history</p>															<div><input id="yes" type="radio" name="group3"> Yes</div>																											<div><input id="no" type="radio" name="group3"> No</div>'
// 					let yes = document.querySelector('#yes')

// 					next.addEventListener('click', ()=>{
// 						if(yes.checked == true) {
// 							body.innerHTML = '<h4>Your status is:</h4><p id="pui" class="text-center">Person Under Investigation</p>'
// 							localStorage.setItem("status", document.querySelector('#pui').innerHTML)
// 							buttonContainer.innerHTML = '<a href="https://www.google.com"><button id="next" type="button" class="btn btn-primary">Next</button></a>'

// 						} else {
// 							body.innerHTML = '<h4>Your status is:</h4><p id="neither" class="text-center">Not Person Under Investigation nor Person Under Monitoring</p>'
// 							localStorage.setItem("status", document.querySelector('#neither').innerHTML)
// 							buttonContainer.innerHTML = '<a href="https://www.google.com"><button id="next" type="button" class="btn btn-primary">Next</button></a>'
// 						}
// 					})

// 				}else {				
// 					body.innerHTML = '<p>3. Did the symptoms occur within 14 days of exposure?</p> 																											<div><input id="yes" type="radio" name="group3"> Yes</div>																										<div><input id="no" type="radio" name="group3"> No</div>'
// 					let yes = document.querySelector('#yes')

// 					next.addEventListener('click', ()=> {
// 						if(yes.checked == true) {
// 							body.innerHTML = '<h4>Your status is:</h4><p id="pui" class="text-center">Person Under Investigation</p>'
// 							localStorage.setItem("status", document.querySelector('#pui').innerHTML)
// 							buttonContainer.innerHTML = '<a href="https://www.google.com"><button id="next" type="button" class="btn btn-primary">Next</button></a>'

// 						} else {
// 							body.innerHTML = '<h4>Your status is:</h4><p id="neither" class="text-center">Not Person Under Investigation nor Person Under Monitoring</p>'
// 							localStorage.setItem("status", document.querySelector('#neither').innerHTML)
// 							buttonContainer.innerHTML = '<a href="https://www.google.com"><button id="next" type="button" class="btn btn-primary">Next</button></a>'
// 						}
// 					})
// 				}
// 			})
// 			}
// 	})	
// })